var fireFoxIceServerConfig = {
  "iceServers": [{
    urls: 'stun:stun.xten.com'
  }, {
    urls: 'turn:numb.viagenie.ca',
    credential: 'muazkh',
    username: 'webrtc@live.com'
  }, {
    urls: 'turn:192.158.29.39:3478?transport=udp',
    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
    username: '28224511:1379330808'
  }, {
    urls: 'turn:192.158.29.39:3478?transport=tcp',
    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
    username: '28224511:1379330808'
  }]
};
var pc_constraints = {
  optional: [{
    DtlsSrtpKeyAgreement: true
  }]
}
var mediaConstraints = {
  mandatory: {
    'OfferToReceiveAudio': true,
    'OfferToReceiveVideo': true
  },
  'offerToReceiveAudio': true,
  'offerToReceiveVideo': true
};

function createPeerConnection() {
  // if (!isFirefox())
  //   window.ownPeerConnection = new webkitRTCPeerConnection(fireFoxIceServerConfig);
  // else
  window.ownPeerConnection = new RTCPeerConnection(fireFoxIceServerConfig);
  console.log("PeerConnection object created succesfully");
  ownPeerConnection.onicecandidate = onIceCandidateEvent;
  ownPeerConnection.ondatachannel = function(ev) {
    console.log('Data channel is created!');
    ev.channel.onopen = function() {
      console.log('Data channel is open and ready to be used.');
    };
    ev.channel.onmessage = function(ev) {
      console.log(ev);
    };
  };
  openDataChannel();
}

function onIceCandidateEvent(e) {
  console.log("On Ice Candidate Handler");
  if (e.candidate) {
    App.signaling.add_candidate(e.candidate, user_to_call);
  }
}

function isFirefox() {
  return typeof InstallTrigger !== 'undefined';
}

function createOfferToOtherUserToConnect(userNameToConnect) {
  window.user_to_call = userNameToConnect;
  ownPeerConnection.createOffer(successOfferCreated, errorOfferCreated);
}

function successOfferCreated(offer) {

  ownPeerConnection.setLocalDescription(offer)
  console.log("Success Offer Created");
  console.log(offer);
  App.signaling.create_offer(offer, user_to_call);
}

function errorOfferCreated(error) {
  console.log(error);
}

function openDataChannel() {
  console.log("OpeningDataChannel");
  var dataChannelOptions = {
    reliable: true,
    bufferedAmount: 100
  };
  window.dataChannel = ownPeerConnection.createDataChannel("eqwqweeesdaddwqdsa");
  dataChannel.onerror = function(error) {
    console.log("Data Channel Error:", error);
  };

  dataChannel.onmessage = function(event) {
    console.log("Got Data Channel Message:", event.data);
  };

  dataChannel.onopen = function() {
    dataChannel.send("Hello World!");
  };

  dataChannel.onclose = function(event) {
    console.log(event);
    console.log("The Data Channel is Closed");
  };
  // dataChannel.onerror = function(error) {
  //   console.log("Error:", error);
  // };
  // dataChannel.onmessage = function(event) {
  //   console.log("Got message:", event.data);
  // };
}


function dataChannelEventsHanler() {
  dataChannel.onerror = function(error) {
    console.log("Data Channel Error:", error);
  };

  dataChannel.onmessage = function(event) {
    console.log("Got Data Channel Message:", event.data);
  };

  dataChannel.onopen = function() {
    dataChannel.send("Hello World!");
  };

  dataChannel.onclose = function() {
    console.log("The Data Channel is Closed");
  };
}

function showLocalCamera() {
  navigator.getUserMedia({
    video: true,
    audio: false
  }, function(stream) {
    var video = document.querySelector('video');
    //inserting our stream to the video tag
		window.localStream = stream;
    video.src = window.URL.createObjectURL(stream);
  }, function(err) {});
}

function addStream(){
	ownPeerConnection.addStream(localStream);
	ownPeerConnection.onaddstream = function (e) {
	$("#remote")[0].src = window.URL.createObjectURL(e.stream);
	};
}
