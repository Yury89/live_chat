class SignalingChannel < ApplicationCable::Channel
  def subscribed
    stream_from current_user
		$redis.set(current_user, '1')
		ActionCable.server.broadcast(current_user,
		 type: :your_name, user_name: current_user)
		$redis.keys.each do |k|
			ActionCable.server.broadcast(k, type: :all_names, user_names: $redis.keys)
		end
  end

  def unsubscribed
    $redis.del(current_user)
  end

	def add_candidate(data)
		candidate = data["candidate"]
		user_name = data["user_name"]
		ActionCable.server.broadcast(user_name,
		 type: :candidate, caller: current_user, candidate: candidate)
	end

	def create_offer(data)
		offer = data["offer"]
		user_name = data["user_name"]
		ActionCable.server.broadcast(user_name,
		 type: :offer, caller: current_user, offer: offer)
	end

	def create_answer(data)
		answer = data["answer"]
		user_name = data["user_name"]
		ActionCable.server.broadcast(user_name,
		 type: :answer, caller: current_user, answer: answer)
	end
end
