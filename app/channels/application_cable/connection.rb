module ApplicationCable
  class Connection < ActionCable::Connection::Base
		identified_by :current_user

	 def connect
		 self.current_user = request.params['user_name']
	 end
  end
end
