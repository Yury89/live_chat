Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	root 'live_chat#index'
	post '/user_login', to: 'live_chat#create'
	get '/chatting', to: 'live_chat#show'
	mount ActionCable.server => '/chat'
end
