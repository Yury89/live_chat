$(document).ready(function() {
  App.signaling = App.cable.subscriptions.create("SignalingChannel", {
    connected: function() {
      console.log("connected");
    },
    disconnected: function() {
      console.log("disconnected");
    },
    received: function(data) {
      switch (data.type) {
        case "your_name":
          yourNameHanler(data);
          break;
        case "all_names":
          allNamesHanler(data);
          break;
        case "candidate":
          candidateHanler(data);
          break;
        case "offer":
          offerHanler(data);
          break;
				case "answer":
					onAnswer(data);
					break;
        default:
          console.log(data);
      }
    },
    add_candidate: function(candidate, user_name) {
      this.perform("add_candidate",{
        candidate: candidate,
        user_name: user_name
      });
    },
    create_offer: function(offer, user_name) {
      this.perform("create_offer",{
        offer: offer,
        user_name: user_name
      });
    },
		create_answer: function(answer, user_name){
			this.perform("create_answer", {answer: answer, user_name: user_name})
		}
  });
});

function yourNameHanler(data) {
  $("#my_name").html("My Name " + data.user_name);
  window.user_name = data.user_name;
  createPeerConnection();
}

function allNamesHanler(data) {
  let res = "All names";
  data.user_names.forEach(function(current_user) {
    if (current_user != user_name)
      res += `<h4>${current_user}</h4>`
  });
  $("#user_list").html(res)
}

function candidateHanler(data) {
  ownPeerConnection.addIceCandidate(new RTCIceCandidate(data.candidate));

}

function offerHanler(data) {
  window.user_to_call = data.caller;
	window.connectedUser = data.caller;
	console.log(connectedUser);
  ownPeerConnection.setRemoteDescription(new RTCSessionDescription(data.offer));
  ownPeerConnection.createAnswer(function(answer) {
    ownPeerConnection.setLocalDescription(answer);
		App.signaling.create_answer(answer, connectedUser);
  }, function(error) {
    alert("oops...error");
  });
}
function onAnswer(data) {
	console.log(data);
	ownPeerConnection.setRemoteDescription(new RTCSessionDescription(data.answer));
}
