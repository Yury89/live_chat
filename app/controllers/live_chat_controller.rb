class LiveChatController < ApplicationController

	def index
		unless $redis.get(cookies[:user_name]).nil?
			redirect_to chatting_path
		end
	end

	def create
		cookies[:user_name] = params[:user_name]
		unless $redis.get(params[:user_name]).nil?
			redirect_to root_path
		end
		redirect_to chatting_path
	end

	def show
	end
end
